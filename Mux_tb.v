`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:04:25 09/13/2015
// Design Name:   Mux
// Module Name:   E:/xilinxProjects/Assignment1/Mux_tb.v
// Project Name:  Assignment1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Mux
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module Mux_tb;

	// Inputs
	reg [7:0] t_addr;
	reg [7:0] t_subr;
	reg t_sign;

	// Outputs
	wire [7:0] t_datainB;

	// Instantiate the Unit Under Test (UUT)
	Mux uut (
		.addr(t_addr), 
		.subr(t_subr), 
		.datainB(t_datainB), 
		.sign(t_sign)
	);

	initial begin
		// Initialize Inputs
		t_addr = 8'b11111111;
		t_subr = 8'b00000000;
		t_sign = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		
		t_sign = 0;
		#100;
		t_sign = 1;
		#100;
		t_sign = 0;
		#100;
		t_sign = 1;
		#100;
		
        
		// Add stimulus here

	end
      
endmodule

