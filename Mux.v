`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:22:48 09/13/2015 
// Design Name: 
// Module Name:    Mux 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Mux(
      addr,
      subr,
    datainB,
	 sign
    );
	 input [7:0] addr;
	 input [7:0] subr;
	 input sign;
	 output [7:0] datainB;
	 reg [7:0] datainB;
always @ (sign or addr or subr)
begin
if(sign == 1'b1) 
 begin
   datainB = subr; //shouldn't 1'b1 be a addr? refer the diagram once.
 end
else
 begin
  datainB = addr;
 end
end

endmodule
/*
	always @ (ADDOut or SUBOut or Sign)
		begin
		case(Sign)
			0:DataInB = SUBOut;
			1:DataInB = ADDOut;
			default: $display("Error");
		endcase
		end
	endmodule
*/